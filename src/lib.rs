#![no_std]
mod palette;
mod random_screen;
mod wasm4;
use core::panic::PanicInfo;
use random_screen::{RandomScreen, RandomScreenImpl};
use seed_creator::{Color, Render, SeedCreator, Update};
use wasm4::*;

#[panic_handler]
fn ph(_info: &PanicInfo) -> ! {
    trace("Panicked");
    loop {}
}

#[no_mangle]
fn start() {
    palette::set_palette([0xFFFFFF, 0x000000, 0xFF0000, 0x0000FF]);
}

static mut SEED_CREATOR: SeedCreator = SeedCreator::new();

#[no_mangle]
fn update() {
    let pointer_event = handle_input();

    update_seed_creator(pointer_event);

    render_seed_creator();

    let accepted = unsafe { SEED_CREATOR.accepted() };
    if accepted {
        render_random_screen();
    }
}

fn render_random_screen() {
    static mut RANDOM_SCREEN: RandomScreenImpl =
        RandomScreenImpl::new(unsafe { SEED_CREATOR.seed() });
    unsafe {
        for _ in 0..100 {
            RANDOM_SCREEN.next_step();
        }
    }
    let pixels = unsafe { RANDOM_SCREEN.get_pixels() };
    let framebuffer = unsafe { &mut *FRAMEBUFFER };
    framebuffer.copy_from_slice(pixels);
}

static mut PREVIOUS_MOUSE: u8 = 0;

fn handle_input() -> Option<(i16, i16)> {
    let previous = unsafe { PREVIOUS_MOUSE };
    let mouse = unsafe { *MOUSE_BUTTONS };
    let pressed_this_frame = mouse & (mouse ^ previous);
    unsafe {
        PREVIOUS_MOUSE = mouse;
    }
    if pressed_this_frame == MOUSE_LEFT {
        let x = unsafe { *MOUSE_X };
        let y = unsafe { *MOUSE_Y };
        return Some((x, y));
    }
    None
}

fn update_seed_creator(pointer_event: Option<(i16, i16)>) {
    if let Some((x_s, y_s)) = pointer_event {
        let x_r = u16::try_from(x_s);
        if let Ok(x) = x_r {
            let y_r = u16::try_from(y_s);
            if let Ok(y) = y_r {
                unsafe {
                    SEED_CREATOR.update((x, y));
                }
            } else {
                trace("Converting mouse click pointer event coordinate x to u16 failed");
            }
        } else {
            trace("Converting mouse click pointer event coordinate x to u16 failed");
        }
    }
}

fn render_seed_creator() {
    let opt_render_list = unsafe { SEED_CREATOR.render() };
    if let Some(render_list) = opt_render_list {
        for t in render_list.texts {
            let color = map_color(&t.color);
            palette::set_draw_color(color);
            text(t.text, t.x, t.y);
        }

        for r in render_list.rects {
            let fill_color = map_color(&r.color);
            let stroke_color = map_color(&r.stroke_color);
            let color = stroke_color << 4 | fill_color;
            palette::set_draw_color(color);
            rect(r.x, r.y, r.width, r.height);
        }
    }
}

fn map_color(color: &Color) -> u16 {
    let color_code = match color {
        Color::Transparent => 0,
        Color::BackgroundColor => 1,
        Color::Color1 => 2,
        Color::Color2 => 3,
        Color::Color3 => 4,
    };
    color_code
}
