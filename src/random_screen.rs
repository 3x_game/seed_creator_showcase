use grand::{Random, MCG};
pub(crate) trait RandomScreen {
    fn next_step(&mut self);
    fn get_pixels(&self) -> &[u8; 6400];
}

pub(crate) struct RandomScreenImpl {
    pixels: [u8; 6400],
    rand: MCG,
}

impl RandomScreenImpl {
    pub(crate) const fn new(seed: u32) -> Self {
        RandomScreenImpl {
            pixels: [0; 6400],
            rand: MCG::from_seed(seed),
        }
    }
}

fn map_random_value(value: u8) -> usize {
    (u16::from(value) * 159 / 255).into()
}

impl RandomScreen for RandomScreenImpl {
    fn next_step(&mut self) {
        let value = self.rand.next();
        let bytes = value.to_be_bytes();
        let x: usize = map_random_value(bytes[0]);
        let y: usize = map_random_value(bytes[1]);

        // see https://wasm4.org/docs/guides/basic-drawing#direct-framebuffer-access
        let index = (160 * y + x) >> 2;

        // always set to color 2
        let color = 0x1;

        // find the right bit for the pixel, since every byte represents 4 pixels
        let bit_position = (x & 0b11) << 1;

        self.pixels[index] |= color << bit_position;
    }

    fn get_pixels(&self) -> &[u8; 6400] {
        &self.pixels
    }
}
