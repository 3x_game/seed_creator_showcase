// src/palette.rs
use crate::wasm4;

pub(crate) fn set_palette(palette: [u32; 4]) {
    unsafe {
        *wasm4::PALETTE = palette;
    }
}

// src/palette.rs
pub(crate) fn set_draw_color(idx: u16) {
    unsafe { *wasm4::DRAW_COLORS = idx.into() }
}
